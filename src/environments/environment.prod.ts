export const environment = {
  production: true,
  apiRoot: 'https://restcountries.eu/rest/v2/'
};
