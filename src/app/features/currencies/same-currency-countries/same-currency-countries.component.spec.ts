import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SameCurrencyCountriesComponent } from './same-currency-countries.component';

describe('SameCurrencyCountriesComponent', () => {
  let component: SameCurrencyCountriesComponent;
  let fixture: ComponentFixture<SameCurrencyCountriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SameCurrencyCountriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SameCurrencyCountriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
