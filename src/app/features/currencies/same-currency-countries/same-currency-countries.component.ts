import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ICurrency } from 'src/app/shared/restcountries-api/interfaces/ICurrency';
import { RestcountriesApiService } from 'src/app/shared/restcountries-api/restcountries-api.service';
import { IDetailedCountry } from 'src/app/shared/restcountries-api/interfaces/IDetailedCountry';

@Component({
  selector: 'app-same-currency-countries',
  templateUrl: './same-currency-countries.component.html',
  styleUrls: ['./same-currency-countries.component.scss']
})
export class SameCurrencyCountriesComponent implements OnInit, OnChanges {

  @Input() selectedCurrency: ICurrency;
  countriesWithSameCurrency: IDetailedCountry[] = null;
  loading = true;

  constructor(private restApiService: RestcountriesApiService) { }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.selectedCurrency) {
      this.loading = true;
      this.restApiService.getCountriesWithSameCurrency(this.selectedCurrency.code).subscribe((countries: IDetailedCountry[]) => {
        this.countriesWithSameCurrency = countries;
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.countriesWithSameCurrency = [];
      });
    }
  }

}
