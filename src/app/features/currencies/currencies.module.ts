import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrenciesListComponent } from './currencies-list/currencies-list.component';
import { SameCurrencyCountriesComponent } from './same-currency-countries/same-currency-countries.component';
import { CurrenciesRoutingModule } from './currencies-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CurrenciesListComponent, SameCurrencyCountriesComponent],
  imports: [
    CommonModule,
    SharedModule,
    CurrenciesRoutingModule
  ],
  exports: [CurrenciesListComponent]
})
export class CurrenciesModule { }
