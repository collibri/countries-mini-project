import { Component, OnInit } from '@angular/core';
import { ICountryCapitalCurrencies } from 'src/app/shared/restcountries-api/interfaces/ICountryCapitalCurrencies';
import { RestcountriesApiService } from 'src/app/shared/restcountries-api/restcountries-api.service';
import { ICurrency } from 'src/app/shared/restcountries-api/interfaces/ICurrency';

@Component({
  selector: 'app-currencies-list',
  templateUrl: './currencies-list.component.html',
  styleUrls: ['./currencies-list.component.scss']
})
export class CurrenciesListComponent implements OnInit {

  loading = true;
  listOfCountries: ICountryCapitalCurrencies[];
  selectedCurrency: ICurrency = null;
  constructor(private restApiService: RestcountriesApiService) { }

  ngOnInit() {
    this.restApiService.getCountriesCapitalsCurrencies().subscribe((list: ICountryCapitalCurrencies[]) => {
      this.listOfCountries = list;
      this.loading = false;
    },
    (error) =>{
      this.listOfCountries = [];
      this.loading = false;
    });
  }

  selectCurrency(currency: ICurrency){
    this.selectedCurrency = currency;
  }

}
