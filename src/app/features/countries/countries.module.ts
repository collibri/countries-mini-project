import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesListComponent } from './countries-list/countries-list.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { RouterModule } from '@angular/router';
import { CountriesRoutingModule } from './countries-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [CountriesListComponent, CountryDetailsComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    CountriesRoutingModule
  ],
  exports: [
    CountriesListComponent
  ]
})
export class CountriesModule { }
