import { Component, OnInit } from '@angular/core';
import { RestcountriesApiService } from '../../../shared/restcountries-api/restcountries-api.service';
import { ICountryCapitalCurrencies } from '../../../shared/restcountries-api/interfaces/ICountryCapitalCurrencies';

@Component({
  selector: 'app-countries-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.scss']
})
export class CountriesListComponent implements OnInit {

  loading = true;
  listOfCountries: ICountryCapitalCurrencies[] = [];
  constructor(private restApiService: RestcountriesApiService) { }

  ngOnInit() {
    this.restApiService.getCountriesCapitalsCurrencies().subscribe((countries: ICountryCapitalCurrencies[]) => {
      this.listOfCountries = countries;
      this.loading = false;
    },
    (error) => {
      this.listOfCountries = [];
      this.loading = false;
    });
  }

}
