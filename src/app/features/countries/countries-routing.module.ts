import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountriesListComponent } from './countries-list/countries-list.component';

const routes: Route[] = [
  {
    path: 'countries-list',
    component: CountriesListComponent,
    children: [
      {
        path: 'country-details/:capitalName',
        component: CountryDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CountriesRoutingModule { }
