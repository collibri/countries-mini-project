import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IDetailedCountry } from 'src/app/shared/restcountries-api/interfaces/IDetailedCountry';
import { switchMap, catchError, map } from 'rxjs/operators';
import { RestcountriesApiService } from 'src/app/shared/restcountries-api/restcountries-api.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss']
})
export class CountryDetailsComponent implements OnInit {

  details: Observable<IDetailedCountry> = null;
  constructor(private route: ActivatedRoute, private restApiService: RestcountriesApiService) { }

  ngOnInit() {
    this.details = this.route.params.pipe(
      switchMap((data) => {
        return this.restApiService.getDetailsForCountryCapital(data.capitalName);
      }),
      map((arrayOfCountries) => {
        return arrayOfCountries.length > 0 ? arrayOfCountries[0] : null;
      }),
      catchError((error) => {
        return of(null);
      })
    );
  }
}
