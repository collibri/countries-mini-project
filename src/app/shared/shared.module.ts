import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestcountriesApiModule } from './restcountries-api/restcountries-api.module';
import { MaterialSharedModule } from './material-shared/material-shared.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialSharedModule,
    RestcountriesApiModule
  ],
  exports: [
    MaterialSharedModule,
    RestcountriesApiModule
  ]
})
export class SharedModule { }
