import { TestBed } from '@angular/core/testing';

import { RestcountriesApiService } from './restcountries-api.service';

describe('RestcountriesApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestcountriesApiService = TestBed.get(RestcountriesApiService);
    expect(service).toBeTruthy();
  });
});
