export interface ICurrency{
  name: string;
  code: string;
  label: string;
}
