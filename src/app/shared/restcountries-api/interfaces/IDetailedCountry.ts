import { ICountryCapitalCurrencies } from './ICountryCapitalCurrencies';

export interface IDetailedCountry extends ICountryCapitalCurrencies {
  population: number;
  nativeName: string;
  subregion: string;
  flag: string;
}
