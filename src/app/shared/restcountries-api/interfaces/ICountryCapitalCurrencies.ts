import { ICurrency } from './ICurrency';

export interface ICountryCapitalCurrencies{
  name: string;
  capital: string;
  currencies: ICurrency[];
}
