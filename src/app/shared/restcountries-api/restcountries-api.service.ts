import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ICountryCapitalCurrencies } from './interfaces/ICountryCapitalCurrencies';
import { Observable } from 'rxjs';
import { IDetailedCountry } from './interfaces/IDetailedCountry';

@Injectable()
export class RestcountriesApiService {

  constructor(private http: HttpClient) { }

  getCountriesCapitalsCurrencies(): Observable<ICountryCapitalCurrencies[]>{
    return this.http.get<ICountryCapitalCurrencies[]>(`${environment.apiRoot}region/europe?fields=name;capital;currencies`);
  }

  getDetailsForCountryCapital(capitalName: string): Observable<IDetailedCountry[]>{
    return this.http.get<IDetailedCountry[]>(`${environment.apiRoot}capital/${capitalName}`);
  }

  getCountriesWithSameCurrency(currencyCode: string): Observable<IDetailedCountry[]>{
    return this.http.get<IDetailedCountry[]>(`${environment.apiRoot}currency/${currencyCode}?fields=name`);
  }
}
