import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestcountriesApiService } from './restcountries-api.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    RestcountriesApiService
  ]
})
export class RestcountriesApiModule { }
